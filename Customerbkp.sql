--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 14.11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    customerid integer,
    gender character varying(10),
    age integer,
    annualincome numeric(8,2),
    spendingscore integer
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (customerid, gender, age, annualincome, spendingscore) FROM stdin;
1	Male	19	15.00	39
2	Male	21	15.00	81
3	Female	20	16.00	6
4	Female	23	16.00	77
5	Female	31	17.00	40
6	Female	22	17.00	76
7	Female	35	18.00	6
8	Female	23	18.00	94
9	Male	64	19.00	3
10	Female	30	19.00	72
11	Male	67	19.00	14
12	Female	35	19.00	99
13	Female	58	20.00	15
14	Female	24	20.00	77
15	Male	37	20.00	13
16	Male	22	20.00	79
17	Female	35	21.00	35
18	Male	20	21.00	66
19	Male	52	23.00	29
20	Female	35	23.00	98
21	Male	35	24.00	35
22	Male	25	24.00	73
23	Female	46	25.00	5
24	Male	31	25.00	73
25	Female	54	28.00	14
26	Male	29	28.00	82
27	Female	45	28.00	32
28	Male	35	28.00	61
29	Female	40	29.00	31
30	Female	23	29.00	87
31	Male	60	30.00	4
32	Female	21	30.00	73
33	Male	53	33.00	4
34	Male	18	33.00	92
35	Female	49	33.00	14
36	Female	21	33.00	81
37	Female	42	34.00	17
38	Female	30	34.00	73
39	Female	36	37.00	26
40	Female	20	37.00	75
41	Female	65	38.00	35
42	Male	24	38.00	92
43	Male	48	39.00	36
44	Female	31	39.00	61
45	Female	49	39.00	28
46	Female	24	39.00	65
47	Female	50	40.00	55
48	Female	27	40.00	47
49	Female	29	40.00	42
50	Female	31	40.00	42
51	Female	49	42.00	52
52	Male	33	42.00	60
53	Female	31	43.00	54
54	Male	59	43.00	60
55	Female	50	43.00	45
56	Male	47	43.00	41
57	Female	51	44.00	50
58	Male	69	44.00	46
59	Female	27	46.00	51
60	Male	53	46.00	46
61	Male	70	46.00	56
62	Male	19	46.00	55
63	Female	67	47.00	52
64	Female	54	47.00	59
65	Male	63	48.00	51
66	Male	18	48.00	59
67	Female	43	48.00	50
68	Female	68	48.00	48
69	Male	19	48.00	59
70	Female	32	48.00	47
71	Male	70	49.00	55
72	Female	47	49.00	42
73	Female	60	50.00	49
74	Female	60	50.00	56
75	Male	59	54.00	47
76	Male	26	54.00	54
77	Female	45	54.00	53
78	Male	40	54.00	48
79	Female	23	54.00	52
80	Female	49	54.00	42
81	Male	57	54.00	51
82	Male	38	54.00	55
83	Male	67	54.00	41
84	Female	46	54.00	44
85	Female	21	54.00	57
86	Male	48	54.00	46
87	Female	55	57.00	58
88	Female	22	57.00	55
89	Female	34	58.00	60
90	Female	50	58.00	46
91	Female	68	59.00	55
92	Male	18	59.00	41
93	Male	48	60.00	49
94	Female	40	60.00	40
95	Female	32	60.00	42
96	Male	24	60.00	52
97	Female	47	60.00	47
98	Female	27	60.00	50
99	Male	48	61.00	42
100	Male	20	61.00	49
101	Female	23	62.00	41
102	Female	49	62.00	48
103	Male	67	62.00	59
104	Male	26	62.00	55
105	Male	49	62.00	56
106	Female	21	62.00	42
107	Female	66	63.00	50
108	Male	54	63.00	46
109	Male	68	63.00	43
110	Male	66	63.00	48
111	Male	65	63.00	52
112	Female	19	63.00	54
113	Female	38	64.00	42
114	Male	19	64.00	46
115	Female	18	65.00	48
116	Female	19	65.00	50
117	Female	63	65.00	43
118	Female	49	65.00	59
119	Female	51	67.00	43
120	Female	50	67.00	57
121	Male	27	67.00	56
122	Female	38	67.00	40
123	Female	40	69.00	58
124	Male	39	69.00	91
125	Female	23	70.00	29
126	Female	31	70.00	77
127	Male	43	71.00	35
128	Male	40	71.00	95
129	Male	59	71.00	11
130	Male	38	71.00	75
131	Male	47	71.00	9
132	Male	39	71.00	75
133	Female	25	72.00	34
134	Female	31	72.00	71
135	Male	20	73.00	5
136	Female	29	73.00	88
137	Female	44	73.00	7
138	Male	32	73.00	73
139	Male	19	74.00	10
140	Female	35	74.00	72
141	Female	57	75.00	5
142	Male	32	75.00	93
143	Female	28	76.00	40
144	Female	32	76.00	87
145	Male	25	77.00	12
146	Male	28	77.00	97
147	Male	48	77.00	36
148	Female	32	77.00	74
149	Female	34	78.00	22
150	Male	34	78.00	90
151	Male	43	78.00	17
152	Male	39	78.00	88
153	Female	44	78.00	20
154	Female	38	78.00	76
155	Female	47	78.00	16
156	Female	27	78.00	89
157	Male	37	78.00	1
158	Female	30	78.00	78
159	Male	34	78.00	1
160	Female	30	78.00	73
161	Female	56	79.00	35
162	Female	29	79.00	83
163	Male	19	81.00	5
164	Female	31	81.00	93
165	Male	50	85.00	26
166	Female	36	85.00	75
167	Male	42	86.00	20
168	Female	33	86.00	95
169	Female	36	87.00	27
170	Male	32	87.00	63
171	Male	40	87.00	13
172	Male	28	87.00	75
173	Male	36	87.00	10
174	Male	36	87.00	92
175	Female	52	88.00	13
176	Female	30	88.00	86
177	Male	58	88.00	15
178	Male	27	88.00	69
179	Male	59	93.00	14
180	Male	35	93.00	90
181	Female	37	97.00	32
182	Female	32	97.00	86
183	Male	46	98.00	15
184	Female	29	98.00	88
185	Female	41	99.00	39
186	Male	30	99.00	97
187	Female	54	101.00	24
188	Male	28	101.00	68
189	Female	41	103.00	17
190	Female	36	103.00	85
191	Female	34	103.00	23
192	Female	32	103.00	69
193	Male	33	113.00	8
194	Female	38	113.00	91
195	Female	47	120.00	16
196	Female	35	120.00	79
197	Female	45	126.00	28
198	Male	32	126.00	74
199	Male	32	137.00	18
200	Male	30	137.00	83
\.


--
-- PostgreSQL database dump complete
--

